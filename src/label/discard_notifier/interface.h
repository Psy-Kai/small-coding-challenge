#pragma once

#include "../../package.h"

namespace label::discard_notifier {

class Interface {
public:
    virtual ~Interface() = default;

    virtual void notify_discarded(const Package &package) = 0;
};

} // namespace label::discard_notifier

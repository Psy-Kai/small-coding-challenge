#pragma once

#include "id_generator/interface.h"

namespace detect {
namespace id_generator {

class IdGenerator : public Interface {
public:
    uint64_t generate_id() override
    {
        return ++m_id;
    }

private:
    uint64_t m_id = 0;
};

} // namespace id_generator

using IdGenerator = id_generator::IdGenerator;

} // namespace detect

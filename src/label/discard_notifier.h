#pragma once

#include <cstdio>

#include "discard_notifier/interface.h"

namespace label {
namespace discard_notifier {

class DiscardNotifier : public Interface {
public:
    void notify_discarded(const Package &package) override
    {
        simulate_notification(package);
    }

private:
    void simulate_notification(const Package &package)
    {
        std::printf("Discard package with id: %lu, width: %lu, height: %lu, timestamp: %lu\n",
                    package.id, package.dimensions.width, package.dimensions.height,
                    package.timestamp.time_since_epoch().count());
    }
};

}; // namespace discard_notifier

using DiscardNotifier = discard_notifier::DiscardNotifier;

} // namespace label

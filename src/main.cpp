#include <cstdio>
#include <thread>

#include "channel.h"
#include "configuration/loader.h"
#include "detect/id_generator.h"
#include "detect/package_detector.h"
#include "detect/task.h"
#include "label/dimension_checker.h"
#include "label/discard_notifier.h"
#include "label/printer.h"
#include "label/task.h"
#include "label/waiter.h"

int main()
{
    std::srand(std::time(nullptr));

    std::puts("Simulation Machine started");

    const auto config = configuration::CompileTimeValueLoader{}.load();

    auto sender_receiver_pair = channel::make_channel<Package, 10>();
    auto detector_thread = std::thread{[&sender_receiver_pair] {
        auto id_generator = detect::IdGenerator{};
        auto package_detector = detect::PackageDetector{id_generator};
        auto task = detect::Task{package_detector, sender_receiver_pair.first};
        task.run();
    }};

    auto labeling_task = std::thread{[&sender_receiver_pair, config] {
        auto dimension_checker = label::DimensionChecker{config.min, config.max};
        auto printer = label::Printer{};
        auto discard_notifier = label::DiscardNotifier{};
        auto waiter = label::Waiter{config.belt_move_time_from_sensor_to_labeling_head};
        auto task = label::Task{dimension_checker, printer, discard_notifier, waiter,
                                sender_receiver_pair.second};
        task.run();
    }};

    /* for this simple challenge I let the threads run indefinitely so they will never join here */
    detector_thread.join();
    labeling_task.join();
}

#pragma once

#include "../../configuration.h"

namespace configuration::loader {

class Interface {
public:
    virtual ~Interface() = default;

    virtual Configuration load() = 0;
};

} // namespace configuration::loader

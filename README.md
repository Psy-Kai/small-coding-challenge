# Small coding Challenge
## Compile

Just run `g++ src/main.cpp` (with any output parameter you like).

## Package Sizes

The dimensions of the package are names `width` and `height`. The assumption was made that a package
should be `height <= width `.

## Configuration

The configuration of tolerances and time to wait until labeling can be done by passing pre processor
values to the compiler. See `src/configuration/loader.h` for possible configuration values.

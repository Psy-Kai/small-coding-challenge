#pragma once

namespace label::waiter {

class Interface {
public:
    virtual ~Interface() = default;
    virtual void wait_until_package_is_under_labeling_head() = 0;
};

} // namespace label::waiter

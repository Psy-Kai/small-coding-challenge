= Overall Design =

The software composes of 2 tasks, the `detector` and the `labeling` task. The `detector` tasks
passes detected packages via a `channel` to the consuming `labeling` task.

= Detector =
The `detector` tasks just waits for a package to be detected and then pushes it to the channel.

= Labeling =
The `labeling` task consumes the packages from the channel. After a configurable amount of time
the package will either be labeled by a (simulated) `labeling machine` or discards them if they are
not in tolerance.

= Special design choices =
The whole application is designed with testability in mind. Also the amount of heap allocations is
kept to a minimum.

Since settings up a package manager like `conan` to get external dependencies is out of scope for
this project, some tradeoffs were made:

*  dependency injection is done manually in the `main` method
*  tests are skipped (no test framework like `googletest` or `catch`)

Additionally no build system was used to keep the project as simple as possible.

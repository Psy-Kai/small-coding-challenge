#pragma once

#include "../../package.h"

namespace detect::package_detector {

static constexpr package::Millimeters MIN_WIDTH = 25000;
static constexpr package::Millimeters MAX_WIDTH = 27000;
static constexpr package::Millimeters MIN_HEIGHT = 9000;
static constexpr package::Millimeters MAX_HEIGHT = 11000;

} // namespace detect::package_detector

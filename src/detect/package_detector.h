#pragma once

#include <cstdlib>
#include <thread>

#include "id_generator/interface.h"
#include "package_detector/fixed_values_for_simulation.h"
#include "package_detector/interface.h"

namespace detect {
namespace package_detector {

template <class TSteadyClock>
class PackageDetector : public package_detector::Interface {
public:
    explicit PackageDetector(id_generator::Interface &id_generator) : m_id_generator{id_generator}
    {}

    Package wait_for_package() override
    {
        return simulate_package_detection();
    }

private:
    Package simulate_package_detection()
    {
        static constexpr auto MAX_SECONDS_TO_WAIT = 5;

        std::this_thread::sleep_for(std::chrono::seconds(std::rand() % MAX_SECONDS_TO_WAIT));

        const auto simulate_width_first = std::rand() % 2;

        const auto value_1 = simulate_width_first ? simulate_width() : simulate_height();
        const auto value_2 = simulate_width_first ? simulate_height() : simulate_width();

        const auto width = std::max(value_1, value_2);
        const auto height = std::min(value_1, value_2);

        return Package{
            package::Dimensions{
                width,
                height,
            },
            m_id_generator.generate_id(),
            TSteadyClock::now(),
        };
    }
    package::Millimeters simulate_width()
    {
        return MIN_WIDTH + (std::rand() % (MAX_WIDTH - MIN_WIDTH));
    }
    package::Millimeters simulate_height()
    {
        return MIN_HEIGHT + (std::rand() % (MAX_HEIGHT - MIN_HEIGHT));
    }

    id_generator::Interface &m_id_generator;
};

} // namespace package_detector

using PackageDetector = package_detector::PackageDetector<std::chrono::steady_clock>;

} // namespace detect

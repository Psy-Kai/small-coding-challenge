#pragma once

#include <chrono>

#include "package.h"

struct Configuration {
    package::Dimensions min;
    package::Dimensions max;
    std::chrono::milliseconds belt_move_time_from_sensor_to_labeling_head;
};

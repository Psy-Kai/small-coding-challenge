#pragma once

#include "../../package.h"

namespace label::dimension_checker {

class Interface {
public:
    virtual ~Interface() = default;

    virtual bool is_valid(const package::Dimensions &dimensions) const = 0;
};

} // namespace label::dimension_checker

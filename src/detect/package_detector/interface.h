#pragma once

#include "../../package.h"

namespace detect::package_detector {

class Interface {
public:
    virtual ~Interface() = default;
    virtual Package wait_for_package() = 0;
};

} // namespace detect::package_detector

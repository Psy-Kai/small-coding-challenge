#pragma once

namespace channel {
namespace sender {

template <class TDerived>
class Interface {
public:
    template <class T>
    void send(T &&value)
    {
        static_cast<TDerived *>(this)->send(std::forward<T>(value));
    }
};

} // namespace sender
namespace receiver {

template <class TReveiver>
struct TypeTraits {
    using ValueType = typename TReveiver::ValueType;
};

template <class TDerived>
class Interface {
public:
    typename TypeTraits<TDerived>::ValueType receive()
    {
        return static_cast<TDerived *>(this)->receive();
    }
};

} // namespace receiver
} // namespace channel

#pragma once

#include <condition_variable>
#include <mutex>
#include <optional>

#include "channel/interface.h"

namespace channel {
namespace detail {

template <class T, std::size_t N>
struct Channel {
    std::mutex mutex;
    std::condition_variable cv;
    /* should use https://github.com/martinmoene/ring-span-lite here  */
    std::array<T, N> queue;
    std::size_t head = 0;
    std::size_t count = 0;
};

} // namespace detail

template <class T, std::size_t N>
class Sender : public sender::Interface<Sender<T, N>> {
public:
    Sender(std::shared_ptr<detail::Channel<T, N>> channel) : m_channel{std::move(channel)} {}

    template <class TT>
    void send(TT &&value)
    {
        static_assert(std::is_same_v<std::decay_t<TT>, T>);

        auto lock = std::unique_lock{m_channel->mutex};
        const auto index = next_pos();
        if (!index) {
            throw std::runtime_error("queue is full");
        }
        m_channel->queue[*index] = std::forward<TT>(value);
        ++(m_channel->count);
        m_channel->cv.notify_one();
    }

private:
    std::optional<std::size_t> next_pos()
    {
        if (m_channel->count == N) {
            return std::nullopt;
        }
        return (m_channel->head + m_channel->count) % N;
    }

    std::shared_ptr<detail::Channel<T, N>> m_channel;
};

template <class T, std::size_t N>
class Receiver : public receiver::Interface<Receiver<T, N>> {
public:
    using ValueType = T;

    Receiver(std::shared_ptr<detail::Channel<T, N>> channel) : m_channel{std::move(channel)} {}

    T receive()
    {
        auto lock = std::unique_lock{m_channel->mutex};
        m_channel->cv.wait(lock, [this] { return m_channel->count > 0; });
        const auto value = m_channel->queue[m_channel->head];
        m_channel->head = (m_channel->head + 1) % N;
        --m_channel->count;
        return value;
    }

private:
    std::shared_ptr<detail::Channel<T, N>> m_channel;
};

namespace receiver {

template <class T, std::size_t N>
struct TypeTraits<Receiver<T, N>> {
    using ValueType = T;
};

} // namespace receiver

template <class T, std::size_t N>
std::pair<Sender<T, N>, Receiver<T, N>> make_channel()
{
    auto channel = std::make_shared<detail::Channel<T, N>>();
    return {Sender<T, N>{channel}, Receiver<T, N>{channel}};
}

} // namespace channel

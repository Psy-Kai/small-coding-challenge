#pragma once

namespace task {

class Interface {
public:
    virtual ~Interface() = default;

    virtual void run() = 0;
};

} // namespace task

#pragma once

#include <chrono>

#include "../channel.h"
#include "../task/interface.h"
#include "package_detector/interface.h"

namespace detect {

template <class TSender>
class Task : public task::Interface {
public:
    Task(package_detector::Interface &package_detector,
         channel::sender::Interface<TSender> &sender) :
        m_package_detector{package_detector}, m_sender{sender}
    {}

    void run() override
    {
        while (true) {
            const auto package = m_package_detector.wait_for_package();
            m_sender.send(package);
        }
    }

private:
    package_detector::Interface &m_package_detector;
    channel::sender::Interface<TSender> &m_sender;
};

} // namespace detect

#pragma once

#include "../../package.h"

namespace label::printer {

class Interface {
public:
    virtual ~Interface() = default;

    virtual void print_label(const Package &package) = 0;
};

} // namespace label::printer

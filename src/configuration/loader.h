#pragma once

#include "loader/interface.h"

namespace configuration {
namespace loader {
namespace detail {

#if CONF_MIN_WIDTH
constexpr package::Millimeters MIN_WIDTH = CONF_MIN_WIDTH;
#else
constexpr package::Millimeters MIN_WIDTH = 25250;
#endif

#if CONF_MAX_WIDTH
constexpr package::Millimeters MAX_WIDTH = CONF_MAX_WIDTH;
#else
constexpr package::Millimeters MAX_WIDTH = 26750;
#endif

#if CONF_MIN_HEIGHT
constexpr package::Millimeters MIN_HEIGHT = CONF_MIN_HEIGHT;
#else
constexpr package::Millimeters MIN_HEIGHT = 9250;
#endif

#if CONF_MAX_HEIGHT
constexpr package::Millimeters MAX_HEIGHT = CONF_MAX_HEIGHT;
#else
constexpr package::Millimeters MAX_HEIGHT = 10750;
#endif

#if CONF_BELT_MOVE_TIME
constexpr std::chrono::milliseconds BELT_MOVE_TIME = std::chrono::milliseconds{CONF_BELT_MOVE_TIME};
#else
constexpr std::chrono::milliseconds BELT_MOVE_TIME = std::chrono::milliseconds{1000};
#endif

} // namespace detail

class CompileTimeValueLoader : public Interface {
public:
    Configuration load() override
    {
        return Configuration{
            package::Dimensions{detail::MIN_WIDTH, detail::MIN_HEIGHT},
            package::Dimensions{detail::MAX_WIDTH, detail::MAX_HEIGHT},
            detail::BELT_MOVE_TIME,
        };
    }
};

} // namespace loader

using CompileTimeValueLoader = loader::CompileTimeValueLoader;

} // namespace configuration

#pragma once

#include <chrono>
#include <thread>

#include "waiter/interface.h"

namespace label {
namespace waiter {

class Waiter : public Interface {
public:
    explicit Waiter(std::chrono::milliseconds wait_time) : m_wait_time{wait_time} {}

    void wait_until_package_is_under_labeling_head() override
    {
        std::this_thread::sleep_for(m_wait_time);
    }

private:
    std::chrono::milliseconds m_wait_time;
};

} // namespace waiter

using Waiter = waiter::Waiter;

} // namespace label

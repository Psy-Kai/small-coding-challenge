#pragma once

#include <chrono>
#include <cstdint>

namespace package {

using Millimeters = uint64_t;

struct Dimensions {
    Millimeters width;
    Millimeters height;
};

} // namespace package

struct Package {
    package::Dimensions dimensions;
    uint64_t id;
    std::chrono::steady_clock::time_point timestamp;
};

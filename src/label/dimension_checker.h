#pragma once

#include "dimension_checker/interface.h"

namespace label {
namespace dimension_checker {

class DimensionChecker : public Interface {
public:
    DimensionChecker(const package::Dimensions &min_dimensions,
                     const package::Dimensions &max_dimensions) :
        m_min_dimensions{min_dimensions}, m_max_dimensions{max_dimensions}
    {}

    bool is_valid(const package::Dimensions &dimensions) const override
    {
        const auto valid_width =
            is_valid(dimensions.width, m_min_dimensions.width, m_max_dimensions.width);
        const auto valid_heigth =
            is_valid(dimensions.height, m_min_dimensions.height, m_max_dimensions.height);
        return valid_width && valid_heigth;
    }

private:
    bool is_valid(const package::Millimeters value, const package::Millimeters min,
                  const package::Millimeters max) const
    {
        return (min <= value) && (value <= max);
    }

    const package::Dimensions m_min_dimensions;
    const package::Dimensions m_max_dimensions;
};

} // namespace dimension_checker

using DimensionChecker = dimension_checker::DimensionChecker;

} // namespace label

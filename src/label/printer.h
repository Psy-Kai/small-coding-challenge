#pragma once

#include <cstdio>

#include "printer/interface.h"

namespace label {
namespace printer {

class Printer : public Interface {
public:
    void print_label(const Package &package) override
    {
        simulate_printing(package);
    }

private:
    void simulate_printing(const Package &package)
    {
        std::printf(
            "Printing label for package with id: %lu, width: %lu, height: %lu, timestamp: %lu\n",
            package.id, package.dimensions.width, package.dimensions.height,
            package.timestamp.time_since_epoch().count());
    }
};

}; // namespace printer

using Printer = printer::Printer;

} // namespace label

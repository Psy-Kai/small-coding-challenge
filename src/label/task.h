#pragma once

#include <chrono>

#include "../channel.h"
#include "../task/interface.h"
#include "dimension_checker/interface.h"
#include "discard_notifier/interface.h"
#include "printer/interface.h"
#include "waiter/interface.h"

namespace label {

template <class TReceiver>
class Task : public task::Interface {
public:
    Task(dimension_checker::Interface &dimension_checker, printer::Interface &printer,
         discard_notifier::Interface &discard_notifier, waiter::Interface &waiter,
         channel::receiver::Interface<TReceiver> &receiver) :
        m_dimension_checker{dimension_checker},
        m_printer{printer},
        m_discard_notifier{discard_notifier},
        m_waiter{waiter},
        m_receiver{receiver}
    {}

    void run() override
    {
        while (true) {
            const auto package = m_receiver.receive();

            m_waiter.wait_until_package_is_under_labeling_head();

            if (!m_dimension_checker.is_valid(package.dimensions)) {
                m_discard_notifier.notify_discarded(package);
                continue;
            }

            m_printer.print_label(package);
        }
    }

private:
    dimension_checker::Interface &m_dimension_checker;
    printer::Interface &m_printer;
    discard_notifier::Interface &m_discard_notifier;
    waiter::Interface &m_waiter;
    channel::receiver::Interface<TReceiver> &m_receiver;
};

} // namespace label

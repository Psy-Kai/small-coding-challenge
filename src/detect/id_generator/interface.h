#pragma once

#include <cstdint>

namespace detect::id_generator {

class Interface {
public:
    virtual ~Interface() = default;
    virtual uint64_t generate_id() = 0;
};

} // namespace detect::id_generator
